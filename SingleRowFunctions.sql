--SINGLE ROW FUNCTIONS
--Character
select upper(first_name) from employees;
select lower(last_name) from employees;
select initcap(email) from employees;
select substr('Mammadova',2,5) from dual;
select instr('Nilufar','f') from dual;
select length('Nilufar Mammadova') from dual;
select replace('Ahmadli','h','kh') from dual;
select concat('Baku',concat(' ','is the capital of Azerbaijan')) from dual;
select trim('   Hello   ') from dual;
select rpad('Caspian Sea',17,'*') from dual;
select lpad('Caspian Sea',18,'*') from dual;
  
--Number
select round(75.347,2) from dual;
select trunc(75.347,2) from dual;
select abs(-19) from dual;
select mod(73,7) from dual;
select ceil(75.347) from dual;
select floor(75.347) from dual;
  
--Date
select sysdate from dual;
select extract(month from to_date(date_, 'MM-DD-YYYY')) from sales_data;
