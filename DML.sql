--DML
--INSERT
--Inserting corresponding values for each tables
INSERT INTO authors (
    author_id,
    first_name,
    last_name,
    birthdate,
    author_email,
    author_phone
) VALUES (
    1,
    'Sarah',
    'Jio',
    '18-02-1978',
    'sarahjio1978@gmail.com',
    0997781457
);
commit;

INSERT INTO books (book_id, title, publication_date, author_id, book_type)
VALUES ( 1,
'Goodnight June',
'27-05-2014',
1,
'Fiction' );
commit;

INSERT INTO checkouts (checkout_id, book_id, checkout_date, due_date)
VALUES ( 1,
1,
'29-06-2018',
'30-07-2018');
commit;

--UPDATE
--Update email in authors table where author id = 1, and update title in books table where book id = 1 and update author id in checkouts table where checkout id = 1 after that commit them
UPDATE authors
SET
    author_email = 'sarahjio19@gmail.com'
WHERE
    author_id = 1;
    commit;
    
UPDATE books
SET
    title = 'Romance'
WHERE
    book_id = 1;

commit;

UPDATE checkouts
SET
    author_id = 1
WHERE
    checkout_id = 1;
commit;

--DELETE
--Delete columns, then commit
DELETE FROM authors
WHERE
    author_id = 1;

commit;

DELETE FROM books
WHERE
    book_id = 1;

commit;

DELETE FROM checkouts
WHERE
    checkout_id = 1;

commit;
