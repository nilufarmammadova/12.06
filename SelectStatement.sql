--SELECT STATEMENT
--PROJECTION CLAUSE
SELECT
    employee_id,
    department_id,
    first_name,
    email
FROM
    employees;

SELECT
    manager_id,
    location_id
FROM
    departments;

--FROM CLAUSE
SELECT * FROM employees;
SELECT * FROM sales_data;
SELECT * FROM departments;

--FILTER CLAUSE
--Using 'where' find name of department where location id greater than 1400
SELECT department_name FROM departments
WHERE location_id > 1400;

--GROUP BY
SELECT count(*), quarter FROM sales_data
GROUP BY quarter;

--HAVING CLAUSE
SELECT first_name, sum (salary) FROM employees
GROUP BY first_name
HAVING sum(salary) > 15000;

--ORDER CLAUSE
SELECT department_id, department_name, location_id FROM departments
ORDER BY department_id DESC;

--FETCH CLAUSE
SELECT * FROM jobs
ORDER BY max_salary
FETCH NEXT 15 ROWS ONLY;
