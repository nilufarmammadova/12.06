--DDL
--CREATE
--Creating three tables authors, books and checkouts
CREATE TABLE authors (
author_id number primary key,
    first_name VARCHAR2(50),
    last_name VARCHAR2(50),
    birthdate DATE,
    author_email VARCHAR2(100)
);

CREATE TABLE books (
book_id number primary key,
title varchar2 (100),
publication_date date,
author_id number, foreign key(author_id) references authors(author_id) );

CREATE TABLE checkouts (
checkout_id number primary key,
book_id number,
checkout_date date,
due_date date, foreign key(book_id) references books(book_id) );

--ALTER
--Using alter statement adding columns for corresponding authors, books and checkouts
ALTER TABLE authors ADD author_phone NUMBER;
ALTER TABLE books ADD book_type VARCHAR2(50);
ALTER TABLE checkouts ADD author_id NUMBER;

--DROP
--Dropping tables
DROP TABLE authors;
DROP TABLE books;
DROP TABLE checkouts;

--CONSTRAINTS
--Adding primary key constraint
ALTER TABLE authors ADD CONSTRAINT pk_authors PRIMARY KEY (author_id);
ALTER TABLE books ADD CONSTRAINT pk_books PRIMARY KEY (book_id);
ALTER TABLE checkouts ADD CONSTRAINT pk_checkout_id PRIMARY KEY (checkout_id);

--KEYS
--Adding unique key
ALTER TABLE authors ADD CONSTRAINT uq_author_email UNIQUE (author_email);
